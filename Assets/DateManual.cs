﻿using UnityEngine;
using System.Collections;
using TheNextFlow.UnityPlugins;
using UnityEngine.UI;

public class DateManual : MonoBehaviour
{

    public GameObject date, time;

    // Use this for initialization
    void Start()
    {
        date.GetComponent<Text>().text = System.DateTime.Now.ToString("dd/MM/yyyy");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDate()
    {
        AndroidNativePopups.OpenDatePickerDialog(
                int.Parse(System.DateTime.Now.ToString("yyyy")), int.Parse(System.DateTime.Now.ToString("MM")), int.Parse(System.DateTime.Now.ToString("dd")),
                (year, month, day) => { date.GetComponent<Text>().text = day + "/" + (month + 1) + "/" + year; });
    }

    public void SetTime()
    {
        AndroidNativePopups.OpenTimePickerDialog(
                int.Parse(System.DateTime.Now.ToString("HH")), int.Parse(System.DateTime.Now.ToString("mm")), true,
                (hours, minutes) => {
                    if (minutes > 9)
                    {
                        time.GetComponent<Text>().text = hours + ":" + minutes;
                    }
                    else
                    {
                        time.GetComponent<Text>().text = hours + ":0" + minutes;
                    }
                });
    }
}
