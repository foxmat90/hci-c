﻿using UnityEngine;
using System.Collections;
using TheNextFlow.UnityPlugins;
using UnityEngine.UI;
using System.Globalization;
using System;

public class InserisciValore : MonoBehaviour {

    public GameObject buttonToBeEnabled;
    public GameObject oraT, glicemiaT, noteT, insulinaT;
    public GameObject ora, glicemia, note;
    public float valoreAtteso = 120;
    public float valoreRilevato;
    public GameObject insulina;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Inserisci()
    {
        //valoreRilevato = float.Parse(glicemia.GetComponent<Text>().text, CultureInfo.InvariantCulture.NumberFormat);
        oraT.GetComponent<Text>().text = ora.GetComponent<Text>().text;
        glicemiaT.GetComponent<Text>().text = glicemia.GetComponent<Text>().text;
        noteT.GetComponent<Text>().text = note.GetComponent<Text>().text;
        //insulinaT.GetComponent<Text>().text = (3+Math.Round((valoreRilevato - valoreAtteso) / 60)).ToString();
        insulinaT.GetComponent<Text>().text = insulina.GetComponent<Text>().text;
        buttonToBeEnabled.SetActive(true);
        AndroidNativePopups.OpenToast("Valore inserito correttamente, unità di insulina: "+ insulinaT.GetComponent<Text>().text, AndroidNativePopups.ToastDuration.Long);
    }
}
