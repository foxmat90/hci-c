﻿using UnityEngine;
using System.Collections;

public class DisablePages : MonoBehaviour {

    public GameObject p1, p2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Disable()
    {
        p1.SetActive(false);
        p2.SetActive(false);
    }
}
